# thunderbird-to-outlook

MailsDaddy Thunderbird to Outlook Converter supports all Windows OS and easily transfers data from Thunderbird to Outlook PST format. It is also helpful to move emails from Thunderbird to PST, EML, MSG, HTML, MHTML, and RTF file formats.
Get more info: https://www.mailsdaddy.com/thunderbird-to-outlook-converter/
